import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

String html = "";
String htmlPreview = "";

String factuurNr = "";

CompanyTemplate selectedCompanyTemplate = CompanyTemplate("-1");
List<CompanyTemplate> companyTemplates =
    List<CompanyTemplate>.empty(growable: true);

List<TimeEntry> timeEntries = List<TimeEntry>.empty(growable: true);

class CompanyTemplate {
  String? id;
  String? factuurNummer;
  String? bedrijfsnaam;
  String? functie;
  String? adres;
  String? woonplaats;
  double? tarief;

  CompanyTemplate(this.id);

  CompanyTemplate.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    factuurNummer = json['id'];
    bedrijfsnaam = json['Bedrijfsnaam'];
    functie = json['Functie'];
    adres = json['Adres'];
    woonplaats = json['Woonplaats'];
    tarief = json['Tarief'];
  }
}

class TimeEntry {
  DateTime? begin;
  DateTime? end;
  int? breakTime;
  String notes = '';

  TimeEntry(this.begin, this.end, this.breakTime, this.notes);

  TimeEntry.fromJson(Map<String, dynamic> json) {
    begin = DateTime.parse(json['begin']);
    end = DateTime.parse(json['end']);
    breakTime = json['breakTime'];
  }
}

Future<void> initAll() async {
  await loadCompanyTemplates();
  await loadHtmlTemplate();
}

Future<void> loadCompanyTemplates() async {
  String json = await http.read(Uri.parse(
      'https://gitlab.com/moerland.rolf/hminvoice/-/raw/main/companies.json?ref_type=heads'));
  Map<String, dynamic> list = await jsonDecode(json);
  companyTemplates = list['companies']
      .map<CompanyTemplate>((dynamic json) => CompanyTemplate.fromJson(json))
      .toList();
  if (companyTemplates.isNotEmpty) {
    selectedCompanyTemplate = companyTemplates[0];
  }
}

Future<void> loadHtmlTemplate() async {
  html = await http.read(Uri.parse(
      'https://gitlab.com/moerland.rolf/hminvoice/-/raw/main/template.html?ref_type=heads'));
}

Future<void> fillHtmlTemplate() async {
  print("starting");
  htmlPreview = html;

  double totalworkedMinutes = 0;

  String getWorkTime(TimeEntry entry) {
    int minutes = entry.end!.difference(entry.begin!).inMinutes;
    minutes -= entry.breakTime!;
    return "${minutes ~/ 60} h ${minutes % 60} m";
  }

  String getShiftPrice(TimeEntry entry) {
    int minutes = entry.end!.difference(entry.begin!).inMinutes;
    minutes -= entry.breakTime!;
    totalworkedMinutes += minutes;
    double price = minutes * (selectedCompanyTemplate.tarief! / 60);
    return price.toStringAsFixed(2);
  }

  String getSubtotal() {
    return "€ ${(totalworkedMinutes * (selectedCompanyTemplate.tarief! / 60)).toStringAsFixed(2)}";
  }

  void replaceKnownWith(String known, String replace) {
    htmlPreview = htmlPreview.replaceAll(known, replace);
  }

  replaceKnownWith("{[Factuurnummer]}", factuurNr);
  replaceKnownWith(
      "{[Bedrijfsnaam]}", selectedCompanyTemplate.bedrijfsnaam ?? '');
  replaceKnownWith("{[Functie]}", selectedCompanyTemplate.functie ?? '');
  replaceKnownWith("{[Adres]}", selectedCompanyTemplate.adres ?? '');
  replaceKnownWith("{[Woonplaats]}", selectedCompanyTemplate.woonplaats ?? '');
  replaceKnownWith("{[Datum]}", DateTime.now().toString().substring(0, 10));
  replaceKnownWith("{[Due]}",
      DateTime.now().add(const Duration(days: 28)).toString().substring(0, 10));

  String data = "";
  for (TimeEntry entry in timeEntries) {
    data += """
        <tr><td>
        ${entry.begin!.toString().substring(0, 10)}
        </td><td>
        ${entry.begin!.toString().substring(11, 16)}
        </td><td>
        ${entry.end!.toString().substring(11, 16)}
        </td><td>
        ${entry.breakTime}
        </td><td>
        ${entry.notes}
        </td><td>
        ${getWorkTime(entry)}
        </td><td>
        ${selectedCompanyTemplate.tarief}
        </td><td>
        ${getShiftPrice(entry)}
        </td></tr>
        """;
  }
  replaceKnownWith("{[Data]}", data);
  replaceKnownWith("{[Subtotaal]}", getSubtotal());
  replaceKnownWith("{[Totaal]}", getSubtotal());
  print("done now");
  print("html is now $htmlPreview");
  File('invoice.html').writeAsString(htmlPreview);
}
