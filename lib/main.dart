import 'package:flutter/material.dart';

import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:hminvoice/data.dart';
import 'package:hminvoice/inputView/main.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Invoice maker',
      theme: ThemeData(
        colorScheme: const ColorScheme.dark(primary: Colors.red),
        useMaterial3: true,
      ),
      home: const Base(),
    );
  }
}

class Base extends StatefulWidget {
  const Base({super.key});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  @override
  State<Base> createState() => _BaseState();
}

class _BaseState extends State<Base> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // TRY THIS: Try changing the color here to a specific color (to
        // Colors.amber, perhaps?) and trigger a hot reload to see the AppBar
        // change color while the other colors stay the same.
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: const Text("Invoice maker"),
        actions: [
          IconButton(
            icon: const Icon(Icons.settings),
            onPressed: () {
              initAll();
            },
          ),
          IconButton(
              onPressed: () {
                fillHtmlTemplate();
                setState(() {});
              },
              icon: const Icon(Icons.handyman_outlined)),
        ],
      ),
      body: ListView(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        clipBehavior: Clip.antiAlias,
        physics: const BouncingScrollPhysics(),
        children: [
          const InputView(),
          Container(
            width: 1200,
            color: Colors.blue,
            child: HtmlWidget(htmlPreview),
          ),
        ],
      ),
    );
  }
}
