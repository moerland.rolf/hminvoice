import 'package:flutter/material.dart';
import 'package:hminvoice/data.dart';

class InputView extends StatefulWidget {
  const InputView({super.key});

  @override
  State<InputView> createState() => _InputViewState();
}

class _InputViewState extends State<InputView> {
  DateTime? begin = DateTime.now();
  DateTime? end = DateTime.now();
  int? breakTime = 0;
  String notes = '';

  @override
  void initState() {
    super.initState();
    initAll().then((value) => setState(() {
          factuurNr = selectedCompanyTemplate.factuurNummer!;
        }));
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(32.0),
      child: SizedBox(
        width: 600,
        child: ListView(
          shrinkWrap: true,
          children: [
            DropdownButton<CompanyTemplate>(
              value: selectedCompanyTemplate,
              onChanged: (value) {
                setState(() {
                  selectedCompanyTemplate = value!;
                });
              },
              items: companyTemplates
                  .map((e) => DropdownMenuItem(
                        value: e,
                        child: Text(
                            "${e.functie ?? ""} @ ${e.bedrijfsnaam ?? ""}"),
                      ))
                  .toList(),
            ),
            Text("id: ${selectedCompanyTemplate.id}"),
            Text("factuurNummerbase: ${selectedCompanyTemplate.factuurNummer}"),
            Text("bedrijfsnaam: ${selectedCompanyTemplate.bedrijfsnaam}"),
            Text("functie: ${selectedCompanyTemplate.functie}"),
            Text("adres: ${selectedCompanyTemplate.adres}"),
            Text("woonplaats: ${selectedCompanyTemplate.woonplaats}"),
            Text("tarief: ${selectedCompanyTemplate.tarief}"),
            TextField(
              controller: TextEditingController(text: factuurNr),
              onChanged: (value) {
                if (!value.startsWith(selectedCompanyTemplate.factuurNummer!)) {
                  value = selectedCompanyTemplate.factuurNummer! + value;
                  setState(() {});
                } else {
                  factuurNr = value;
                }
              },
              decoration: const InputDecoration(
                labelText: "Factuurnr",
              ),
            ),
            const SizedBox(height: 32),
            addTimeEntry(),
            const SizedBox(height: 32),
            const Text("timeEntries:"),
            ...timeEntries.map((e) => buildTimeEntry(e)),
          ],
        ),
      ),
    );
  }

  Container buildTimeEntry(TimeEntry timeEntry) {
    Duration total = timeEntry.end!
        .subtract(Duration(minutes: timeEntry.breakTime!))
        .difference(timeEntry.begin!);

    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("begin: ${timeEntry.begin.toString().substring(0, 16)}"),
                Text("end: ${timeEntry.end.toString().substring(0, 16)}"),
                Text(
                    "duration: ${total.inHours}:${total.inMinutes.remainder(60).toString().padLeft(2, '0')}")
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Text("breakTime: ${timeEntry.breakTime}"),
                Text("notes: ${timeEntry.notes}"),
                Text(
                    "total: €${(total.inMinutes * selectedCompanyTemplate.tarief! / 60).toStringAsFixed(2)}")
              ],
            ),
            ElevatedButton(
                onPressed: () {
                  timeEntries.remove(timeEntry);
                  setState(() {});
                },
                child: const Icon(Icons.delete))
          ],
        ),
      ),
    );
  }

  Container addTimeEntry() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  InkWell(
                    onTap: () async {
                      DateTime? actual;
                      actual = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now()
                            .subtract(const Duration(days: 365 * 10)),
                        lastDate:
                            DateTime.now().add(const Duration(days: 365 * 10)),
                      );
                      if (actual == null) return;
                      TimeOfDay? actualTime;
                      if (!mounted) return;
                      actualTime = await showTimePicker(
                          context: context, initialTime: TimeOfDay.now());
                      if (actualTime == null) return;
                      actual = DateTime(actual.year, actual.month, actual.day,
                          actualTime.hour, actualTime.minute);
                      begin = actual;
                      setState(() {});
                    },
                    child: Text("begin: ${begin.toString().substring(0, 16)}"),
                  ),
                  InkWell(
                    onTap: () async {
                      DateTime? actual;
                      actual = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime.now()
                            .subtract(const Duration(days: 365 * 10)),
                        lastDate:
                            DateTime.now().add(const Duration(days: 365 * 10)),
                      );
                      if (actual == null) return;
                      TimeOfDay? actualTime;
                      if (!mounted) return;
                      actualTime = await showTimePicker(
                          context: context, initialTime: TimeOfDay.now());
                      if (actualTime == null) return;
                      actual = DateTime(actual.year, actual.month, actual.day,
                          actualTime.hour, actualTime.minute);
                      end = actual;
                      setState(() {});
                    },
                    child: Text("end: ${end.toString().substring(0, 16)}"),
                  ),
                ],
              ),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () async {
                        int? tempbreak = await showDialog<int>(
                          context: context,
                          builder: (context) => SimpleDialog(
                            title: const Text("breakTime"),
                            children: [
                              for (int i = 0; i <= 60; i += 5)
                                SimpleDialogOption(
                                  onPressed: () {
                                    Navigator.pop(context, i);
                                  },
                                  child: Text("$i"),
                                ),
                            ],
                          ),
                        );
                        if (tempbreak == null) return;
                        breakTime = tempbreak;
                        setState(() {});
                      },
                      child: Text("breakTime: $breakTime"),
                    ),
                  ),
                  Expanded(
                    child: TextField(
                      onChanged: (value) {
                        notes = value;
                      },
                      decoration: const InputDecoration(
                        labelText: "notes",
                      ),
                    ),
                  ),
                ],
              ),
              TextButton(
                onPressed: () {
                  timeEntries.add(TimeEntry(
                    begin,
                    end,
                    breakTime,
                    notes,
                  ));
                  setState(() {});
                },
                child: const Text("add"),
              ),
            ],
          )),
    );
  }
}
